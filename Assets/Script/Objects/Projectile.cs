﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public float speed;
	public Rigidbody2D rb;

	public virtual void Start () {
		if (rb != null) {
			rb = gameObject.GetComponent<Rigidbody2D> ();
		}
	}
	
	// Update is called once per frame
	public virtual void Update () {
	
	}
	public virtual void OnCollisionEnter2D(Collision2D collision){
		//TODO colpire il personaggio
		if (collision.gameObject.tag == "Player") {
			GameManager.getManager ().killPlayer ();
		}
	}
}
