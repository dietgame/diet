﻿using UnityEngine;
using System.Collections;

public class Fruit : Projectile {

	private Vector3 sprint = Vector3.zero;
	private int bounceCount=0;
	private int maxBounces = 5;


	// Use this for initialization
	public override void Start () {
		base.Start ();
	}

	public void setSprint(Vector2 sprint){
		//Velocità iniziale al rigidbody
		rb.velocity =sprint;
	}

	public override void OnCollisionEnter2D(Collision2D collision){
		/* Comportamento della frtutta:
		 * rimbalzare per un tot poi distruggersi
		 */

		base.OnCollisionEnter2D (collision);
		bounceCount ++;
		if (bounceCount >= maxBounces) {
			Destroy(gameObject);
		}
	}

	public override void  Update(){
	}

}
