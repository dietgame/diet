﻿using UnityEngine;
using System.Collections;

public class ChasingCamera : MonoBehaviour {

	public GameObject target;
	public float force;

	private Rigidbody2D body;

	void Start () {
		body = GetComponent<Rigidbody2D>();
	}

	void Update () {
		var distance = (Vector2) (target.transform.position - transform.position);
		body.AddForce(distance * force);
	}
}
