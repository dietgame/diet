﻿using UnityEngine;
using System.Collections;

public class TestMovement : MonoBehaviour {

	public float force;

	private Rigidbody2D body;

	void Start() {
		body = GetComponent<Rigidbody2D>();
	}

	void Update () {
		var direction = new Vector2(Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical")) * force;
		body.AddForce(direction);
	}
}
