﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	static GameManager manager;
	
	public Vector3 spawnPoint = Vector3.zero;
	public GameObject player;
	// Use this for initialization
	void Start () {
		manager = this;
		spawnPoint = player.transform.position;
		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static GameManager getManager(){
		return manager;
	}



	public void killPlayer(){
		player.transform.position = spawnPoint;
	}
	
}
