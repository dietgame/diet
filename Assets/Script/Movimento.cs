﻿using UnityEngine;
using System.Collections;

public class Movimento : MonoBehaviour {

	public float speed = 5f;
	Rigidbody2D physBody;

	// Use this for initialization
	void Start () {
		physBody = this.GetComponent<Rigidbody2D> ();
		//FIXME, altrimenti ruota anche la camera
		physBody.freezeRotation = true;
	}
	
	// Update is called once per frame
	void Update () {
		physBody.AddForce(new Vector2(Input.acceleration.x,Input.acceleration.y) * Time.deltaTime * speed);
	}
}
